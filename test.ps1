function Get-HResult {
    <#
    .SYNOPSIS
    get HRESULT formatted as hex
    .DESCRIPTION
    gets HResult of latest exception, converted to hex, returned as string
    .EXAMPLE
    Get-HResult
    gets HResult of latest exception, converted to hex, returned as string
    .INPUTS
    None.
    .OUTPUTS
    System.String
    #>

    #get $error[0] and return exception.hresult converted to hex
    return ('{0:X}' -f $error[0].Exception.HResult).Trim()
}

Write-Host "hello world!"
Write-Host "byebye world!"
Write-Host "$env:COMPUTERNAME"

Write-Host "test ob indoviduelle git config für bubble-me zieht"